﻿using System;
using System.Collections.Generic;

namespace CorrMediaPlayerData.Model
{
    public class Album
    {
        public Album() {}

        public virtual int Id { get; set; }
        public virtual List<Artist> Artist { get; set; }
        public virtual List<Audio> Tracks { get; set; }
        public virtual string Name { get; set; }
        public virtual int Year { get; set; }
        public virtual DateTime? Length { get; set; }
        public virtual short Gain { get; set; }

    }
}
