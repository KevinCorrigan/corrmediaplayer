﻿namespace CorrMediaPlayerData.Model
{
    public class Artist 
    {
        public Artist() { }

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
