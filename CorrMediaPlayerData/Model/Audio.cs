﻿using System;
using System.Collections.Generic;

namespace CorrMediaPlayerData.Model
{
    public class Audio 
    {
        public Audio () { }
        public virtual int Id { get; set; }
        public virtual Publisher Publisher { get; set; }
        public virtual List<Composer> Composers { get; set; }
        public virtual List<Artist> Artists { get; set; }
        public virtual string Name { get; set; }
        public virtual short Disc { get; set; }
        public virtual int TrackNumber { get; set; }
        public virtual short Gain { get; set; }
        public virtual short Bpm { get; set; }
        public virtual int Year { get; set; }
        public virtual DateTime Time { get; set; }
    }
}
