﻿namespace CorrMediaPlayerData.Model
{
    public class Composer
    {
        public Composer () { }

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
