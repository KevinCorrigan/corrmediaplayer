﻿namespace CorrMediaPlayerData.Model
{
    public class Director// : AModel
    {
        public Director() {}
        
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
