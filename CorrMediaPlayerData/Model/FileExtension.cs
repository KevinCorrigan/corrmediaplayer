﻿namespace CorrMediaPlayerData.Model
{
    public class FileExtension 
    {
        public FileExtension() {}
        
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual MediaType MediaType { get; set; }
        public virtual bool Supported { get; set; }
    }
}
