﻿namespace CorrMediaPlayerData.Model
{
    public class Genre 
    {
        public Genre() { }
        
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
