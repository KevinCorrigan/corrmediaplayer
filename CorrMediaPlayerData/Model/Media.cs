﻿using System;

namespace CorrMediaPlayerData.Model
{
    public class Media
    {
        public Media() {}

        public virtual int Id { get; set; }
        public virtual MediaType MediaType { get; set; }
        public virtual FileExtension Extension { get; set; }
        public virtual Genre Genre { get; set; }
        public virtual Audio Audio { get; set; }
        public virtual Podcast Podcast { get; set; }
        public virtual Video Video { get; set; }
        public virtual string FileName { get; set; }
        public virtual DateTime FileTime { get; set; }
        public virtual long FileSize { get; set; }
        public virtual string Comment { get; set; }
        public virtual short BitRate { get; set; }
        public virtual short PlayCount { get; set; }
        public virtual short Rating { get; set; }
        public virtual DateTime? LastPlayed { get; set; }
        public virtual DateTime? DateAdded { get; set; }
        public virtual DateTime? LastUpdated { get; set; }
    }
}
