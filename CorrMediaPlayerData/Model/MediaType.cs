﻿namespace CorrMediaPlayerData.Model
{
    public class MediaType 
    {
        public MediaType() { }

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
