﻿using System;

namespace CorrMediaPlayerData.Model
{
    public class Podcast 
    {
        public Podcast () { }
        
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime PublishDate { get; set; }
        public virtual int Year { get; set; }
        public virtual DateTime Length { get; set; }
    }
}
