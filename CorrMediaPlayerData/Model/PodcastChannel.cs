﻿using System.Collections.Generic;

namespace CorrMediaPlayerData.Model
{
    public class PodcastChannel
    {
        public PodcastChannel() { }

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual List<Podcast> Podcasts { get; set; }
    }
}
