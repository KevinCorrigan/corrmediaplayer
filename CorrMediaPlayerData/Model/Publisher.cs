﻿namespace CorrMediaPlayerData.Model
{
    public class Publisher
    {
        public Publisher() {}

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
