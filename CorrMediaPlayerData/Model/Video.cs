﻿using System;
using System.Collections.Generic;

namespace CorrMediaPlayerData.Model
{
    public class Video
    {
        public Video() {}

        public virtual int Id { get; set; }
        public virtual List<Director> Director { get; set; }
        public virtual string Name { get; set; }
        public virtual int Year { get; set; }
        public virtual DateTime Length { get; set; }
        public virtual string Dimention { get; set; }
    }
}
