﻿using System.Collections.Generic;
using CorrMediaPlayerData.Model;

namespace CorrMediaPlayerData.Repository
{
    public abstract class ARepository
    {
        public abstract int Add(AModel model);

        public abstract void Update(AModel model);

        public abstract AModel GetById(int id);

        public abstract IList<object> GetAll();

        public abstract void Delete(AModel model);
    }
}
