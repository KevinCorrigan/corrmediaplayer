﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public static class AlbumRepository
    {
        public static int Add(Model.Album model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int) session.Save(model);
            }
        }

        public static void Update(Model.Album model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Album GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Album>(id);
            }
        }

        public static IList<Model.Album> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Album>().List();
            }
        }

        public static void Delete(Model.Album model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
