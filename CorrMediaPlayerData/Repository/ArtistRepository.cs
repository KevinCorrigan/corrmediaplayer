﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public static class ArtistRepository
    {
        public static int Add(Model.Artist model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int) session.Save(model);
            }
        }

        public static void Update(Model.Artist model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Artist GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Artist>(id);
            }
        }

        public static IList<Model.Artist> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Artist>().List();
            }
        }

        public static void Delete(Model.Artist model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
