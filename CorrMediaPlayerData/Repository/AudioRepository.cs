﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public static class AudioRepository
    {
        public static int Add(Model.Audio model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int) session.Save(model);
            }
        }

        public static void Update(Model.Audio model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Audio GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Audio>(id);
            }
        }

        public static IList<Model.Audio> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Audio>().List();
            }
        }

        public static void Delete(Model.Audio model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
