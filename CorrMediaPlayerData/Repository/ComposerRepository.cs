﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public static class ComposerRepository
    {
        public static int Add(Model.Composer model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int) session.Save(model);
            }
        }

        public static void Update(Model.Composer model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Composer GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Composer>(id);
            }
        }

        public static IList<Model.Composer> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Composer>().List();
            }
        }

        public static void Delete(Model.Composer model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
