﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public class DirectorRepository
    {
        public static int Add(Model.Director model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int)session.Save(model);
            }
        }

        public static void Update(Model.Director model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Director GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Director>(id);
            }
        }

        public static IList<Model.Director> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Director>().List();
            }
        }

        public static void Delete(Model.Director model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
