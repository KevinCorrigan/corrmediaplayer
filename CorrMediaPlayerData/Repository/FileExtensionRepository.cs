﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public class FileExtensionRepository
    {
        public static int Add(Model.FileExtension model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int)session.Save(model);
            }
        }

        public static void AddAll(List<Model.FileExtension> models) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Transaction.Begin();
                foreach (var model in models)
                    session.Save(model);
                session.Transaction.Commit();
            }
        }

        public static void Update(Model.FileExtension model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Transaction.Begin();
                session.Update(model, model.Id);
                session.Transaction.Commit();
            }
        }

        public static Model.FileExtension GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.FileExtension>(id);
            }
        }

        public static IList<Model.FileExtension> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.FileExtension>().List();
            }
        }

        public static void Delete(Model.FileExtension model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
