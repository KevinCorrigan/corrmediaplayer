﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public class GenreRepository
    {
        public static int Add(Model.Genre model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int)session.Save(model);
            }
        }

        public static void Update(Model.Genre model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Genre GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Genre>(id);
            }
        }

        public static IList<Model.Genre> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Genre>().List();
            }
        }

        public static void Delete(Model.Genre model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
