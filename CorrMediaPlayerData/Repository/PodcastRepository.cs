﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public class PodcastRepository
    {
        public static int Add(Model.Podcast model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int)session.Save(model);
            }
        }

        public static void Update(Model.Podcast model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Podcast GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Podcast>(id);
            }
        }

        public static IList<Model.Podcast> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Podcast>().List();
            }
        }

        public static void Delete(Model.Podcast model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
