﻿using System.Collections.Generic;
using NHibernate;

namespace CorrMediaPlayerData.Repository
{
    public class PublisherRepository
    {
        public static int Add(Model.Publisher model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return (int)session.Save(model);
            }
        }

        public static void Update(Model.Publisher model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Update(model);
            }
        }

        public static Model.Publisher GetById(int id) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.Get<Model.Publisher>(id);
            }
        }

        public static IList<Model.Publisher> GetAll() {
            using (ISession session = NHibernateFactory.OpenSession()) {
                return session.QueryOver<Model.Publisher>().List();
            }
        }

        public static void Delete(Model.Publisher model) {
            using (ISession session = NHibernateFactory.OpenSession()) {
                session.Delete(model);
            }
        }
    }
}
