﻿using System;
using System.IO;
using CorrMediaPlayerData.Model;

namespace CorrMediaPlayerDataUtilities.FileFolder
{
    public class ScanFile
    {
        public static FileInfo Info (string path) {
            FileInfo info = new FileInfo(path);
            return info;
        }
    }
}