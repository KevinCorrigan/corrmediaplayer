﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CorrMediaPlayerDataUtilities.FileFolder
{
    public class ScanResult
    {
        public ScanResult() {
            Files = new List<string>();
            Folders = new List<string>();
        }

        public List<string> Files { get; set; }
        public List<string> Folders { get; set; }

        public new string ToString() {
            var builder = new StringBuilder();

            builder.AppendLine("Folders:");
            foreach (var rpath in Folders)
                builder.AppendLine(rpath);
            builder.AppendLine();
            builder.AppendLine("Files:");
            foreach (var rpath in Files)
                builder.AppendLine(rpath);
            return builder.ToString();
        }
    }

    public static class ScanFolder
    {
        [STAThread]
        public static ScanResult Scan(string folderPath) {
            var result = new ScanResult {
                Folders = Directory.EnumerateDirectories(folderPath).ToList(),
                Files = Directory.EnumerateFiles(folderPath).ToList()
            };
            return result;
        }

        public static ScanResult DeepScan(string folderPath, int depth = -1, ScanResult preResult = null) {
            if (depth == 0) return preResult;

            var result = new ScanResult {
                Folders = Directory.EnumerateDirectories(folderPath).ToList(),
                Files = Directory.EnumerateFiles(folderPath).ToList(),
            };
            result.Files.Add("");

            var deepScanResults = new ScanResult();
            foreach (var path in result.Folders) {
                var newResults = DeepScan(path, depth - 1, result);
                deepScanResults.Folders.AddRange(newResults.Folders);
                deepScanResults.Files.AddRange(newResults.Files);
            }

            result.Folders.AddRange(deepScanResults.Folders);
            result.Files.AddRange(deepScanResults.Files);

            return result;
        }
    }
}
