﻿using System.Collections.Generic;
using System.Linq;
using CorrMediaPlayerData.Model;
using CorrMediaPlayerData.Repository;

namespace CorrMediaPlayerDataUtilities.Media
{
    public static class Identification
    {

        public static bool IsMedia(string extension) {
            List<FileExtension> extensions = FileExtensionRepository.GetAll().ToList();
            return extensions.Any(
                ex => string.Equals(ex.Name, extension, System.StringComparison.OrdinalIgnoreCase));
        }
        
        public static bool IsSupportedMedia(string extension) {
            List<FileExtension> supportedExtensions = 
                FileExtensionRepository.GetAll().Where(ex => ex.Supported).ToList();
            return supportedExtensions.Any(
                ex => string.Equals(ex.Name, extension, System.StringComparison.OrdinalIgnoreCase));
        }

        public static bool IsAudioFormat(string extension) {
            List<FileExtension> supportedExtensions =
                FileExtensionRepository.GetAll().Where(ex => ex.MediaType.Name.Equals(
                        "Audio", System.StringComparison.OrdinalIgnoreCase)).ToList();
            return supportedExtensions.Any(
                ex => string.Equals(ex.Name, extension, System.StringComparison.OrdinalIgnoreCase));
        }

        public static bool IsVideoFormat(string extension) {
            List<FileExtension> supportedExtensions =
                FileExtensionRepository.GetAll().Where(ex => ex.MediaType.Name.Equals(
                        "Audio", System.StringComparison.OrdinalIgnoreCase)).ToList();
            return supportedExtensions.Any(
                ex => string.Equals(ex.Name, extension, System.StringComparison.OrdinalIgnoreCase));
        }


    }
}
