﻿using System;
using System.IO;
using CorrMediaPlayerData.Model;
using CorrMediaPlayerDataUtilities.FileFolder;

namespace CorrMediaPlayerDataUtilities.Media
{
    public static class ScanMedia
    {
        public static CorrMediaPlayerData.Model.Media ScanFileAt(string path, Shell32.Folder folder, Shell32.FolderItem file) {
            // Extended File Properties (Shell32)
            // Genre            = 16
            // Kind (MediaType) = 11
            // BitRate          = 28
            // Rating           = 19
            // Comments         = 24

            FileInfo fileInfo = ScanFile.Info(path);
            CorrMediaPlayerData.Model.Media media = new CorrMediaPlayerData.Model.Media {
                Id = 0,
                PlayCount = 0,
                FileName = fileInfo.Name,
                FileSize = fileInfo.Length,
                FileTime = fileInfo.CreationTime,
                Extension = new FileExtension { Name = fileInfo.Extension },
                LastUpdated = DateTime.Now,
                LastPlayed = null,
                DateAdded = null,
                MediaType = new MediaType() { Id = 0, Name = folder.GetDetailsOf(file, 11) },
                Audio = null,
                Podcast = null,
                Video = null,
                Genre = new Genre() { Id = 0, Name = folder.GetDetailsOf(file, 16) },
                Comment = folder.GetDetailsOf(file, 24),
            };
            short bitrate;
            media.BitRate = short.TryParse(folder.GetDetailsOf(file, 28), out bitrate) ? bitrate : (short)-1;

            short rating;
            media.Rating = short.TryParse("" + folder.GetDetailsOf(file, 19)[0], out rating) ? rating : (short)0;

            return media;
        }
    }
}
