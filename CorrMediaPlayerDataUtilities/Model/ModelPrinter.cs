﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CorrMediaPlayerDataUtilities.Model
{
    public static class ModelPrinter
    {
        public static void PrintSingle(Type modelType, object model) {
            PropertyInfo[] properties = modelType.GetProperties();

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(modelType.Name + ":");

            foreach (var info in properties) {
                builder.Append(info.Name + ": ");
                builder.Append(info.GetValue(model) + "\n");
            }
            Console.WriteLine(builder.ToString());
        }

        public static void PrintAll(Type modelType, IList<object> models) {
            foreach (var model in models) {
                PrintSingle(modelType, model);
            }

        }
    }
}
