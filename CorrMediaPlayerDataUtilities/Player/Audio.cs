﻿using System;
using System.IO;
using System.Threading.Tasks;
using CSCore;
using CSCore.Codecs;
using CSCore.SoundOut;

namespace CorrMediaPlayerDataUtilities.Player
{
    public enum PlaybackMode
    {
        Forward,
        Reverse,
        Shuffle,      
    }

    public class Audio
    {
        #region Private Properties
        private PlaybackMode _PlaybackMode { get; set; }
        private ISoundOut _SoundOut { get; set; }
        private float _Volume { get; set; }
        private Random _Random { get; set; }
        private PlaylistUtil.Playlist _Playlist { get; set; }
        private int _PlaylistPosition { get; set; }
        private bool _Initialized { get; set; }
        private bool _SongSwitching { get; set; }
        private const int _TIMEOUT = 1000;
        #endregion


        #region Public Properties
        public PlaybackMode PlaybackMode {
            get { return _PlaybackMode; }
            set
            {
                if (!value.Equals(_PlaybackMode))
                    _PlaybackMode = value;
            }    
        }

        public PlaylistUtil.Playlist Playlist {
            get { return _Playlist; }
            set
            {
                if (!value.Equals(_Playlist)) {
                    _Playlist = value;
                    _PlaylistPosition = -1;
                    Next();
                }
            }
        }

        public float Volume {
            get { return _Volume; }
            set
            {
                if (!value.Equals(_Volume)) {
                    if (_Initialized)
                        _SoundOut.Volume = value;
                    _Volume = value;
                }
            }
        }
        #endregion


        #region Constructors
        public Audio() {
            _SoundOut = _GetSoundOut();
            _Volume = 1.0f;

            _PlaybackMode = PlaybackMode.Forward;
            _Playlist = null;
            _PlaylistPosition = -1;
            _Initialized = false;
            _SongSwitching = false;

            _SoundOut.Stopped += AudioEnded;
        }
        #endregion


        #region Private Methods
        private static ISoundOut _GetSoundOut() {
            if (WasapiOut.IsSupportedOnCurrentPlatform)
                return new WasapiOut();
            return new DirectSoundOut();
        }

        private void _UpdatePlaylistPosition() {
            switch (_PlaybackMode) {
                case PlaybackMode.Forward:
                    ++_PlaylistPosition;
                    break;
                case PlaybackMode.Reverse:
                    --_PlaylistPosition;
                    break;
                case PlaybackMode.Shuffle:
                    _PlaylistPosition = _Random.Next(_Playlist.Count);
                    break;
            }

            if (_PlaylistPosition < 0)
                _PlaylistPosition = _Playlist.Count-1;
            else if (_PlaylistPosition >= _Playlist.Count)
                _PlaylistPosition = 0;
        }
        #endregion


        #region Public Methods
        public void PlayAll(PlaylistUtil.Playlist playlist) {
            _Playlist = playlist;
            Next();
        }

        public void Next() {
            if (_SongSwitching) return;
            _SongSwitching = true;
            PlaylistUtil.PlaylistEntry entry;

            bool start;
            int fails = 0;
            do {
                _UpdatePlaylistPosition();
                start = _Playlist.TryGetValue(_PlaylistPosition, out entry);
                if (!start) ++fails;
            } while (!start || fails > _TIMEOUT);

            Stop();
            try {
                Start(entry.Path);
            }
            catch (FileNotFoundException ex) {
                _SongSwitching = false;
                Next();
            }
        }

        public void Previous() {
            var currentMode = PlaybackMode;
            PlaybackMode = PlaybackMode.Reverse;
            Next();
            PlaybackMode = currentMode;
        }

        public void Start(string audioFile) {
            var soundSource = CodecFactory.Instance.GetCodec(audioFile);
            _SoundOut.Initialize(soundSource);
            _SoundOut.Play();
            _SoundOut.Volume = _Volume;

            _Initialized = true;
            _SongSwitching = false;
        }

        public void Stop() {
            _SoundOut.Stop();
            _SoundOut.WaitForStopped();
        }

        public void Pause() {
            _SoundOut.Pause();
        }

        public void Resume() {
            _SoundOut.Resume();
        }

        public void Restart() {
            Seek(0);
        }
        
        public long Seek(long offset) {
            if (!_SoundOut.WaveSource.CanSeek)
                return -1;
            var newPosition = _SoundOut.WaveSource.Position + offset;

            if (newPosition > _SoundOut.WaveSource.Length)
                newPosition = _SoundOut.WaveSource.Length - 1;
            else if (newPosition < 0)
                newPosition = 0;

            _SoundOut.WaveSource.SetPosition(new TimeSpan(newPosition));

            return newPosition;
        }

        public void Seek(TimeSpan position) {
            _SoundOut.WaveSource.SetPosition(position);
        }
        #endregion


        #region Private Events
        private void AudioEnded(object sender, PlaybackStoppedEventArgs e) {
            if (_Playlist != null ) {
                Task.Run(() => { Next(); });
            }
        }
        #endregion
    }
}
