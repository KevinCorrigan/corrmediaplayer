﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CorrMediaPlayerDataUtilities.Player
{
    public static class PlaylistUtil
    {         
        public class Playlist : Dictionary<int, PlaylistEntry>
        {
            private new void Add(int position, PlaylistEntry info) {
                base.Add(position, info);
            }

            public void Add(int position, int length, string artist, string title, string path) {
                Add(position, new PlaylistEntry(length, artist, title, path));
            }
        }

        public class PlaylistEntry
        {
            public int Length { get; }
            public string Artist { get; }
            public string Title { get; }
            public string Path { get; }

            public PlaylistEntry(int length, string artist, string title, string path) {
                Length = length;
                Artist = artist;
                Title = title;
                Path = path;
            }
        }

        public static class Importers
        {
            public interface IImporter
            {
                string Path { get; }
                string[] Extensions { get; }
                Playlist Import();
            }

            private class M3UImporter : IImporter
            {
                private string _path;
                private string[] _extensions = new string[2] {".m3u", ".m3u8"};
                private const string _HEADER_TAG = "#EXTM3U";
                private const string _TRACK_INFO_TAG = "#EXTINF:";
                
                private M3UImporter() { }
                public M3UImporter(string path) {
                    _path = path;
                }
                
                string IImporter.Path => _path;
                string[] IImporter.Extensions => _extensions;

                Playlist IImporter.Import() {
                    // FORMAT: 

                    // #EXTM3U 
                    // #EXTINF: length(seconds),Artist Name - Track Title
                    // Path

                    // Path can be exact    : D:\Documents\Music\Areosmith\2001-Young Lust - The Aerosmith  Anthology\A05 Rag Doll.mp3
                    // Path can be relative : 2001-Young Lust - The Aerosmith  Anthology\A05 Rag Doll.mp3
                    //                   or : A05 Rag Doll.mp3
                    // Path can be a url    : http://www.fake-stream.com:8080
                    // Path can point to other M3U files (Ignored for now)
                    
                    var extension = _path.Substring(_path.Length - 5);
                    if (!extension.Equals(_extensions[0]) && !extension.Equals(_extensions[1]))
                        return null;

                    var playlist = new Playlist();

                    // Start Parsing here
                    using (var file = File.OpenRead(_path))
                    using (var stream = new StreamReader(file)) {

                        // Check for the header tag
                        var line = stream.ReadLine();
                        if (line == null || !line.Equals(_HEADER_TAG))
                            return null;

                        // Parse the rest of the file
                        int positon = 0;
                        while (!stream.EndOfStream) {
                            line = stream.ReadLine();
                            if (line == null) return null;

                            int length = 0;
                            string artist = string.Empty, title = string.Empty, path = string.Empty;

                            // Read info tag
                            if (line.StartsWith(_TRACK_INFO_TAG)) {
                                int start = line.IndexOf(':', _TRACK_INFO_TAG.Length-1);
                                var artistTitle = line.Substring(start).Split(',', '-', ':');

                                int.TryParse(artistTitle[0], out length);
                                artist = artistTitle[1];
                                title = artistTitle[2];
                            }

                            // Read file path
                            line = stream.ReadLine();
                            if (line == null ||
                                string.IsNullOrEmpty(artist) || 
                                string.IsNullOrEmpty(title))
                                return null;

                            playlist.Add(positon++, length, artist, title, line);
                        }
                    }
                    return playlist;
                }
            }

            public static IImporter CreateImporter(string path) {
                return new M3UImporter(path);
            }
        }

        public static class Exporters
        {
            public interface IExporter
            {
                string Path { get; }
                string Extension { get; }
                void Export(ref Playlist playlist);
            }

            private class M3UExporter : IExporter
            {
                private string _path;
                private const string _EXTENSION = ".m3u";
                private const string _HEADER_TAG = "#EXTM3U";
                private const string _TRACK_INFO_TAG = "#EXTINF:";

                public M3UExporter(string path) {
                    _path = path;
                }

                string IExporter.Path => _path;
                string IExporter.Extension => _EXTENSION;

                void IExporter.Export(ref Playlist playlist) {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(_HEADER_TAG);

                    foreach (var entry in playlist.Values) {
                        builder.AppendLine($"{_TRACK_INFO_TAG}{entry.Length},{entry.Artist} - {entry.Title}");
                        builder.AppendLine(entry.Path);
                    }

                    using (var file = File.Create(_path))
                    using (var stream = new StreamWriter(file)) {
                        stream.WriteAsync(builder.ToString());
                    }
                }
            }

            public static IExporter CreateExporter(string path) {
                return new M3UExporter(path);
            }
        }

        public static Playlist Import(string path) {
            var importer = Importers.CreateImporter(path);
            return importer.Import();
        }

        public static void Export(string path, ref Playlist playlist) {
            var exporter = Exporters.CreateExporter(path);
            exporter.Export(ref playlist);
        }
    }
}
