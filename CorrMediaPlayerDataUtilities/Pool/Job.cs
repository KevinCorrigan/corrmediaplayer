﻿using System;
using System.Collections.Generic;

namespace CorrMediaPlayerDataUtilities.Pool
{
    public interface IJob<T>
    {
        bool Completed { get; }

        void Start();
        T Result();
    }

    public class JobOneArg<T> : IJob<T>
    {
        private Delegate _Job { get; set; }
        private object[] _Args { get; set; }
        private bool _Completed { get; set; }
        private T _Result { get; set; }

        public JobOneArg(Delegate job, params object[] args) {
            _Job = job;
            _Args = args;
        }

        bool IJob<T>.Completed => _Completed;

        void IJob<T>.Start() {
            _Result = (T)_Job.DynamicInvoke(_Args);
            _Completed = true;
        }

        T IJob<T>.Result() {
            return _Result;
        }
    }
}
