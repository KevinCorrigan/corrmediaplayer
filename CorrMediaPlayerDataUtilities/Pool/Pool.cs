﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CorrMediaPlayerDataUtilities.Pool
{
    public class Pool<T, U> where T : IJob<T>
    {
        private List<object[]> _args { get; set; }
        private U[] _pool;
        private Queue _jobQueue;

        public Pool() {
            _pool = new U[Environment.ProcessorCount];
            
            // TODO: Fill job queue

            // TODO: Start work
            
            // TODO: Do Work 
            
            // TODO: End Work
            
        }
    }
}
