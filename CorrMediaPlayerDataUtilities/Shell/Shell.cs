﻿using System.Runtime.InteropServices;

namespace CorrMediaPlayerDataUtilities.Shell
{
    public static class Shell
    {
        private static readonly object _syncRoot = new object();
        private static volatile Shell32.Shell _shell = null;
        
        public static Shell32.Shell Instance {
            get
            {
                if (_shell == null)
                    lock (_syncRoot) {
                        if (_shell == null)
                            _shell = new Shell32.Shell();
                    }
                return _shell;
            }
        }
        
        //Must be called before program exits
        public static void Release() {
            if (_shell != null)
                Marshal.ReleaseComObject(_shell);
        }
    }
}
 