**CorrMediaPlayer** is a media player designed to be controlled anywhere on your local network using a web interface or on your local machine using the desktop application.

Planned Features                         | Completed Features |                    |
-----------------------------------------|--------------------|--------------------|---------------------
Play many popular audio types            |    ✓               |                    |
Play many popular video types            |    X               |                    |
Stream audio and video from the internet |    X               |                    |
Track playback statistics                |    X               |                    |
Track media information                  |    X               |                    |
Controlled through a desktop application |    X               |                    |
Controlled through a web interface       |    X               |                    |
Modular Highly customizable UI           |    X               |                    |
Playlist Support                         |    ✓               |                    |


UI Preview (HTML)|
:-----------------------------------------------------------------------:|
![CorrMediaPlayer_LayoutPreview.PNG](https://bitbucket.org/repo/gLy7xn/images/4035086527-CorrMediaPlayer_LayoutPreview.PNG)|