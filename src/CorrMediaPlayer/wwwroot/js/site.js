﻿// Write your Javascript code.

window.onload = function() {
    addCustomEvents();
}

function addCustomEvents() {
    $(".rectangular-radio").each(function() {
        this.onclick = onTabChanged;
    });

    var $el = $("[id^='info-'].rectangular-radio");
    $el.each(function () {
        this.addEventListener("click", switchInfoViewPage);
    });

    document.getElementById("button-close").addEventListener("click", closeCorrMediaPlayer);
}

function onTabChanged() {
    var $sender = $(window.event.target);
    var $tabs =$sender[0].parentElement.children;
        
    for (var i = 0; i < $tabs.length; ++i) {
        var $tab = $($tabs[i]);
        if ($tab.hasClass("radio-checked")) {
            $tab.toggleClass("radio-checked");
        }
    }
        
    if (!$sender.hasClass("radio-checked")) {
        $sender.toggleClass("radio-checked");
    }
}

function switchInfoViewPage() {
    var $sender = $(window.event.target);
    var id = $sender.attr('id');

    switch (id) {
        case "info-view-statistics-tab": case "info-view-global": case "info-view-local":
            if (!$(document.getElementById("info-view-statistics-tab")).hasClass("radio-checked"))
                return;

            var $global = $(document.getElementById("info-view-global"));
            if ($global.hasClass("radio-checked")) {
                $(document.getElementById("info-view-stats-global").hidden = false);
                $(document.getElementById("info-view-stats-local").hidden = true);
            } else {
                $(document.getElementById("info-view-stats-global").hidden = true);
                $(document.getElementById("info-view-stats-local").hidden = false);
            }
            $(document.getElementById("info-view-artist").hidden = true);
            $(document.getElementById("info-view-album").hidden = true);
            $(document.getElementById("info-view-song").hidden = true);
            break;
        case "info-view-artist-tab":
            $(document.getElementById("info-view-stats-global").hidden = true);
            $(document.getElementById("info-view-stats-local").hidden = true);
            $(document.getElementById("info-view-artist").hidden= false);
            $(document.getElementById("info-view-album").hidden= true);
            $(document.getElementById("info-view-song").hidden= true);
            break;
        case "info-view-album-tab":
            $(document.getElementById("info-view-stats-global").hidden = true);
            $(document.getElementById("info-view-stats-local").hidden = true);
            $(document.getElementById("info-view-artist").hidden= true);
            $(document.getElementById("info-view-album").hidden= false);
            $(document.getElementById("info-view-song").hidden= true);
            break;
        case "info-view-song-tab":
            $(document.getElementById("info-view-stats-global").hidden = true);
            $(document.getElementById("info-view-stats-local").hidden = true);
            $(document.getElementById("info-view-artist").hidden= true);
            $(document.getElementById("info-view-album").hidden= true);
            $(document.getElementById("info-view-song").hidden = false);
            break;
        default:
            break;
    }
}